package com.mjurinic.root.streamerinho;

public interface SourceAsyncResponse {
    void processFinish(Boolean response);
}
