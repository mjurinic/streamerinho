package com.mjurinic.root.streamerinho;

import android.app.Activity;

import java.lang.ref.WeakReference;

public class ContextPlayerActivity {

    private static WeakReference<PlayerActivity> mPlayerActivity;

    public static void updateActivity(PlayerActivity activity) {
        mPlayerActivity = new WeakReference<PlayerActivity>(activity);
    }

    public static WeakReference<PlayerActivity> getPlayerActivity() { return mPlayerActivity; }
}
