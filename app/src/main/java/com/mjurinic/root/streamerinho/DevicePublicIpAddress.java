package com.mjurinic.root.streamerinho;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created by root on 23.08.15..
 */
public class DevicePublicIpAddress {

    private static HttpResponse response;

    public static String getPublicIpAddress() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet("http://ifcfg.me/ip");

        try {
            response = httpclient.execute(httpget);
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpEntity entity = response.getEntity();

        if (entity != null) {
            try {
                return EntityUtils.toString(entity).replace("\n", "");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
