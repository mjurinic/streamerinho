package com.mjurinic.root.streamerinho;

public interface PlayerAsyncResponse {
    void processFinish(Boolean response);
}
