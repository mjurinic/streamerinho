package com.mjurinic.root.streamerinho;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import java.net.InetAddress;
import java.net.UnknownHostException;

import MyUdpServer.ReceivedPacket;
import MyUdpServer.SerializedObject;
import MyUdpServer.UdpServer;
import messagetypes.FindStreamSource;
import messagetypes.IdentifierNotUsable;
import messagetypes.Multimedia;
import messagetypes.PleaseForward;
import messagetypes.RelayList;
import messagetypes.RelayListRequest;
import messagetypes.RequestStreaming;
import messagetypes.ShutDown;
import messagetypes.SocketIdentifier;
import messagetypes.SourceReady;
import messagetypes.StreamSourceData;

public class PlayerRetrieverConnection extends AsyncTask<String, String, Boolean> {

    public PlayerAsyncResponse delegate = null;

    private Context playerContext;
    private ProgressDialog pDialog;
    private String errorDialogMessage;
    private Thread thrDataFlow;

    private String streamIdentifier;
    private InetAddress playerIP, retrieverIP, relayIP, publicPlayerIP, publicSourceIP;
    private int playerPort, retrieverPort, relayPort, publicSourcePort;
    private StreamSourceData sourceData;
    private boolean isRelayActive = false;

    private UdpServer server;
    private PlayerDataFlow playerDataFlow;

    public PlayerRetrieverConnection(Context playerContext) {
        this.playerContext = playerContext;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    publicPlayerIP = InetAddress.getByName(DevicePublicIpAddress.getPublicIpAddress());
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pDialog = new ProgressDialog(playerContext);
        pDialog.setMessage("Checking connection code...");
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();
    }

    @Override
    protected void onProgressUpdate(String... message) {
        super.onProgressUpdate();

        if (message[0].equals("Dismiss")) pDialog.dismiss();

        pDialog.setMessage(message[0]);
    }

    @Override
    protected Boolean doInBackground(String... params) {
        server = new UdpServer();

        streamIdentifier = params[0];
        playerIP = DeviceIpAddress.getAddress();
        playerPort = server.getSocketPortNumber();

        try {
            retrieverIP = InetAddress.getByName("188.166.52.29");
            retrieverPort = 9090;
        } catch (UnknownHostException e) {
            e.printStackTrace();

            errorDialogMessage = "Server offline.";

            return false;
        }

        if (!findStreamSource()) {
            errorDialogMessage = "Unable to find stream source.";

            return false;
        }

        if (!connectToSource()) {
            errorDialogMessage = "Unable to pair with the source device.";

            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (result) delegate.processFinish(result);

        if (pDialog != null) pDialog.dismiss();

        final Boolean res = result;
        AlertDialog errorDialog;

        try {
            thrDataFlow.wait();
            thrDataFlow.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (!result) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(playerContext);

            dialogBuilder.setTitle("Error")
                    .setMessage(errorDialogMessage)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            delegate.processFinish(res);
                            dialog.dismiss();
                        }
                    });

            errorDialog = dialogBuilder.create();
            errorDialog.show();
        }
        else {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(playerContext);

            dialogBuilder.setMessage("Live stream ended!")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            delegate.processFinish(res);
                            dialog.dismiss();
                        }
                    });

            errorDialog = dialogBuilder.create();
            errorDialog.show();
        }
    }

    private boolean connectToSource() {
        System.out.println("[CHECKPOINT #1]");
        if (checkDirectConnectivity()) {
            publishProgress("Dismiss");

            thrDataFlow = new Thread(new PlayerDataFlow(streamIdentifier, playerPort + 1));
            thrDataFlow.start();

            listenParamRequests();

            return true;
        }

        /*
        System.out.println("[CHECKPOINT #2]");
        if (waitForSource()) {
            //start streaming phase
        }
        */

        System.out.println("[CHECKPOINT #2]");
        if (goBothWays()) {
            publishProgress("Dismiss");
            isRelayActive = true;

            thrDataFlow = new Thread(playerDataFlow);
            thrDataFlow.start();

            listenParamRequests();

            return true;
        }

        return false;
    }

    private void listenParamRequests() {
        /**
         * Listen for stream shutdown / change params / etc ??
         */
        ReceivedPacket receivedPacket;
        Object receivedObject;

        server.setSocketTimeOut(0);

        while (true) {
            receivedPacket = server.receivePacket();

            if (receivedPacket != null) {
                receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                if (receivedObject instanceof ShutDown) {
                    try {
                        thrDataFlow.wait();
                        thrDataFlow.interrupt();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    return;
                }
            }
        }
    }

    /**
     * Sends MSG_REQUEST_RELAY_LIST to the retriever and waits for
     * MSG_RELAY_LIST
     *
     * After receiving a list of relay servers, contacts the first one
     * on the list (EZ WAY) send MSG_REQUEST_STREAMING (to the relay form now one)
     * and wait for MSG_MULTIMEDIA (?) -> start streaming phase
     *
     * @return boolean
     */
    private boolean goBothWays() {
        server.setSocketTimeOut(5000);

        publishProgress("Contacting relay server...");

        server.sendPacket(retrieverIP, retrieverPort, SerializedObject.Serialize(new RelayListRequest()));

        ReceivedPacket receivedPacket = server.receivePacket();

        if (receivedPacket != null) {
            Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

            if (receivedObject instanceof RelayList) {
                System.out.println("[RECEIVED] MSG_RELAY_LIST");

                RelayList relayList = (RelayList) receivedObject;
                SocketIdentifier relayServer = relayList.getRelayServers().get(0);

                if (relayList.getRelayServers().size() == 0) {
                    return false;
                }

                relayIP = relayServer.getIp();
                relayPort = relayServer.getPort();

                RequestStreaming requestStreaming = new RequestStreaming(publicPlayerIP, playerPort);
                PleaseForward forwardRequestStreaming = new PleaseForward(streamIdentifier, publicSourceIP.toString(), publicSourcePort, requestStreaming);

                /**
                 * Also maps NAT
                 */
                try {
                    server.setSocketTimeOut(1750);
                    int cnt = 0;

                    playerDataFlow = new PlayerDataFlow(streamIdentifier, playerPort + 1);
                    playerDataFlow.setRelayIP(relayIP);
                    playerDataFlow.setRelayPort(relayPort);
                    playerDataFlow.setSourceIP(publicSourceIP);
                    playerDataFlow.setSourcePort(publicSourcePort);
                    playerDataFlow.MapDeviceToNAT();

                    while (cnt < 15) {
                        System.out.println("[SENDING] MSG_PLEASE_FORWARD");

                        server.sendPacket(InetAddress.getByName(relayIP.getHostAddress()), relayPort, SerializedObject.Serialize(forwardRequestStreaming));

                        receivedPacket = server.receivePacket();

                        if (receivedPacket != null) {
                            receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                            if (receivedObject instanceof Multimedia) {
                                System.out.println("[RECEIVED] MSG_MULTIMEDIA (goBothWays)");

                                return true;
                            }
                        }

                        ++cnt;
                    }
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;
    }

    /**
     * Waiting 5000ms for MSG_ACCEPTABLE_PARAMETERS or MSG_MULTIMEDIA
     * from the source device in case player has public IP address
     *
     * @return boolean
     */
    private boolean waitForSource() {
        server.setSocketTimeOut(5000);

        System.out.println("[LISTENING] Waiting for source connection");

        publishProgress("Waiting for 'source' connection...");

        ReceivedPacket receivedPacket = server.receivePacket();

        if (receivedPacket != null) {
            Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

            if (receivedObject instanceof Multimedia) {
                System.out.println("[RECEIVED] MSG_MULTIMEDIA");

                return true;
            }

            /**
             * Not yet implemented, might not be needed
             *
                if (receivedObject instanceof AcceptableParameters) {
                    return true;
                }
             */
        }

        return false;
    }

    /**
     * Send MSG_REQUEST_STREAMING 5x every 2000ms
     * until MSG_SOURCE_READY is received
     *
     * If nothing arrives the destination IP address is
     * not public and a 3rd party device is needed (aka. relay server)
     *
     * @return boolean
     */
    private boolean checkDirectConnectivity() {
        server.setSocketTimeOut(1750);
        int cnt = 0;

        publishProgress("Checking 'sources' availability...");

        while (cnt < 5) {
            System.out.println("[SENDING] MSG_REQUEST_STREAMING --> " + publicSourceIP +":"+ publicSourcePort);

            try {
                server.sendPacket(InetAddress.getByName(sourceData.getSourceIdentifier().getPrivateIp().getHostAddress()), sourceData.getSourceIdentifier().getPrivatePort(), SerializedObject.Serialize(new RequestStreaming(playerIP, playerPort)));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            ReceivedPacket receivedPacket = server.receivePacket();

            if (receivedPacket != null) {
                Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                if (receivedObject instanceof SourceReady) {
                    System.out.println("[RECEIVED] MSG_SOURCE_READY");

                    return true;
                }
            }

            ++cnt;
        }

        return false;
    }

    /**
     * Send MSG_FIND_STREAM_SOURCE 5x every 2000ms
     * until MSG_STREAM_SOURCE_DATA is received
     *
     * @return boolean
     */
    private boolean findStreamSource() {
        server.setSocketTimeOut(2000);
        int cnt = 0;

        while (cnt < 5) {
            System.out.println("[SENDING] MSG_FIND_STREAM_SOURCE --> " + retrieverIP +":"+ retrieverPort);

            server.sendPacket(retrieverIP, retrieverPort, SerializedObject.Serialize(new FindStreamSource(streamIdentifier)));

            ReceivedPacket receivedPacket = server.receivePacket();

            if (receivedPacket != null) {
                Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                if (receivedObject instanceof StreamSourceData) {
                    sourceData = (StreamSourceData) receivedObject;

                    publicSourceIP = sourceData.getSourceIdentifier().getIp();
                    publicSourcePort = sourceData.getSourceIdentifier().getPort();

                    System.out.println("[RECEIVED] MSG_SOURCE_DATA");

                    return true;
                }

                if (receivedObject instanceof IdentifierNotUsable) {
                    System.out.println("[RECEIVED] MSG_IDENTIFIER_NOT_USABLE");

                    return false;
                }
            }

            ++cnt;
        }

        return false;
    }

    public void stopDataFlow() {
        if (thrDataFlow != null) {
            System.out.println("[DEBUG] thrDataFlow exists");

            if (isRelayActive) {
                if (playerDataFlow != null) {
                    //playerDataFlow.sendShutDownSignal();
                }
            }
            else {
                System.out.println("[DEBUG] No relay");

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            server.sendPacket(InetAddress.getByName(publicSourceIP.getHostAddress()), publicSourcePort, SerializedObject.Serialize(new ShutDown(streamIdentifier)));
                            System.out.println("[SENDING] MSG_SHUT_DOWN");
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

            try {
                synchronized (thrDataFlow) {
                    //playerDataFlow.setThrActive(false);
                    thrDataFlow.wait();
                    thrDataFlow.interrupt();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("[DEBUG] --After interrupt--");
        }
    }
}
