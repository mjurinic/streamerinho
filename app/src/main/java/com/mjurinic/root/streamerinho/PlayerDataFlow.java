package com.mjurinic.root.streamerinho;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import java.net.InetAddress;
import java.net.UnknownHostException;

import MyUdpServer.ReceivedPacket;
import MyUdpServer.SerializedObject;
import MyUdpServer.UdpServer;
import messagetypes.Multimedia;
import messagetypes.NAThello;
import messagetypes.PleaseForward;
import messagetypes.ShutDown;

public class PlayerDataFlow implements Runnable {

    private InetAddress sourceIP, relayIP = null;
    private int newPortNum, sourcePort, relayPort;
    private UdpServer mServer;

    private String identifier;
    private volatile boolean stop = false, toggle = false;

    private ImageView ivPlayer;
    private PlayerActivity playerActivityRef;
    private volatile byte[] frameData;

    public PlayerDataFlow(String identifier, int newPortNum) {
        this.identifier = identifier;

        playerActivityRef = ContextPlayerActivity.getPlayerActivity().get();
        ivPlayer = playerActivityRef.getIvPlayer();

        mServer = new UdpServer(newPortNum);
    }

    @Override
    public void run() {
        mServer.setSocketTimeOut(5000);

        while (!stop) {
            ReceivedPacket receivedPacket = mServer.receivePacket();

            if (stop) break;

            if (receivedPacket != null) {
                Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                if (receivedObject instanceof Multimedia) {
                    System.out.println("[RECEIVED] MSG_MULTIMEDIA");

                    Multimedia multimedia = (Multimedia) receivedObject;
                    frameData = multimedia.getFrameData();

                    if (frameData != null) {
                        final Bitmap bmp = BitmapFactory.decodeByteArray(frameData, 0, frameData.length);

                        playerActivityRef.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ivPlayer.setImageBitmap(bmp);
                            }
                        });
                    }
                    else {
                        System.out.println("[DEBUG] FRAME IS NULL");
                    }

                    if (stop) break;
                }
            }
        }
    }

    public void MapDeviceToNAT() {
        /**
         * NAT Map the Relay
         */
        try {
            System.out.println("[SENDING] NAT_HELLO -> " + relayIP.getHostAddress() +":"+ relayPort);

            NAThello hello1 = new NAThello(relayIP, 12345);
            mServer.sendPacket(InetAddress.getByName(relayIP.getHostAddress()), relayPort, SerializedObject.Serialize(hello1));
            mServer.setSocketTimeOut(2000);

            ReceivedPacket receivedPacket = mServer.receivePacket();

            if (receivedPacket != null) {
                Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                if (receivedObject instanceof NAThello) {
                    NAThello rcvHello = (NAThello) receivedObject;
                    NAThello hello = new NAThello(InetAddress.getByName(relayIP.getHostAddress()),rcvHello.getPort());

                    newPortNum = rcvHello.getPort();

                    // mislim da ovo nema smisla ponovno slati al nek bude za sad
                    mServer.sendPacket(InetAddress.getByName(relayIP.getHostAddress()), relayPort, SerializedObject.Serialize(hello));
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void setFrameData(byte[] frameData) {
        this.frameData = frameData;

        final Bitmap bmp = BitmapFactory.decodeByteArray(frameData, 0, frameData.length);

        playerActivityRef.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ivPlayer.setImageBitmap(bmp);
            }
        });
    }

    public void setRelayIP(InetAddress relayIP) {
        this.relayIP = relayIP;
    }

    public void setRelayPort(int relayPort) {
        this.relayPort = relayPort;
    }

    public int getNewPortNum() {
        return newPortNum;
    }

    public void setSourceIP(InetAddress sourceIP) {
        this.sourceIP = sourceIP;
    }

    public void setSourcePort(int sourcePort) {
        this.sourcePort = sourcePort;
    }

    public void stopThread() {
        stop = true;
    }

    public void ccThread() {
        toggle = !toggle;
    }
}
