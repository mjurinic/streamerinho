package com.mjurinic.root.streamerinho;

import java.lang.ref.WeakReference;

public class ContextSourceActivity {

    private static WeakReference<StreamActivity> mStreamActivity;

    public static void updateActivity(StreamActivity activity) {
        mStreamActivity = new WeakReference<StreamActivity>(activity);
    }

    public static WeakReference<StreamActivity> getStreamActivity() { return mStreamActivity; }
}
