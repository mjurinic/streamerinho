package com.mjurinic.root.streamerinho;

import org.apache.http.entity.SerializableEntity;

import java.net.InetAddress;
import java.net.UnknownHostException;

import MyUdpServer.ReceivedPacket;
import MyUdpServer.SerializedObject;
import MyUdpServer.UdpServer;
import messagetypes.Multimedia;
import messagetypes.NAThello;
import messagetypes.Ping;
import messagetypes.PleaseForward;
import messagetypes.ShutDown;
import messagetypes.StreamRemove;

public class SourceDataFlow implements Runnable {

    private int newSocketPort;
    private String identifier;
    private volatile boolean stop = false, toggle = false;

    private UdpServer server;
    private InetAddress playerIP = null, relayIP = null;
    private int relayPort, firstMult = 0;
    private volatile int playerPort = 0;
    private boolean directTransfer, firstMultimedia = true;

    public SourceDataFlow(String identifier, InetAddress relayIP, int relayPort, int newSocketPort, boolean directTransfer) {
        System.out.println("Starting DataFlow thread!");

        this.relayIP = relayIP;
        this.relayPort = relayPort;
        this.newSocketPort = newSocketPort;
        this.identifier = identifier;
        this.directTransfer = directTransfer;
    }

    public SourceDataFlow(String identifier, InetAddress playerIP, int playerDataPort, boolean directTransfer) {
        this.identifier = identifier;
        this.playerIP = playerIP;
        this.playerPort = playerDataPort;
        this.directTransfer = directTransfer;
    }

    @Override
    public void run() {
        while (playerIP == null && playerPort == 0 && !stop) {
            System.out.println("Player IP:PORT are null.");

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        server = new UdpServer(newSocketPort);

        try {
            server.sendPacket(InetAddress.getByName("188.166.52.29"), 9090, SerializedObject.Serialize(new StreamRemove(identifier)));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        if (directTransfer) {
            while (!stop) {
                byte[] frameData = CameraPreview.getFrameData();
                Multimedia multimedia = new Multimedia(identifier, frameData);

                try {
                    server.sendPacket(InetAddress.getByName(playerIP.getHostAddress()), playerPort, SerializedObject.Serialize(multimedia));
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            while (!stop) {
                while (toggle);

                //msg pls forward has to be sent
                byte[] frameData = CameraPreview.getFrameData();

                if (frameData == null) continue;

                Multimedia multimedia = new Multimedia(identifier, frameData);

                if (firstMult >= 5) {
                    PleaseForward pleaseForward = new PleaseForward(identifier, playerIP.toString().replace("/", ""), playerPort, multimedia);

                    System.out.println("[DATA FLOW] Sending packet to: " + relayIP + ":" + relayPort + " --> Forwarding to: " + playerIP + ":" + (playerPort));
                    server.sendPacket(relayIP, relayPort, SerializedObject.Serialize(pleaseForward));
                }
                else {
                    ++firstMult;

                    PleaseForward pleaseForward = new PleaseForward(identifier, playerIP.toString().replace("/", ""), playerPort, multimedia);
                    server.sendPacket(relayIP, relayPort, SerializedObject.Serialize(pleaseForward));

                    System.out.println("[DATA FLOW #1] Sending packet to: " + relayIP + ":" + relayPort + " --> Forwarding to: " + playerIP + ":" + (playerPort));

                    server.setSocketTimeOut(2000);
                    ReceivedPacket receivedPacket = server.receivePacket();

                    if (receivedPacket != null) {
                        Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                        if (receivedObject instanceof NAThello) {
                            System.out.println("[RECEIVED] NAT_HELLO inside DataFlow -> Setting new port: " + ((NAThello) receivedObject).getPort());
                            playerPort = ((NAThello) receivedObject).getPort();

                            firstMult = 5;
                        }
                    }
                }
            }
        }

        /**
         * Wait for shut down signal
         */
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setPlayerIP(InetAddress playerIP) {
        this.playerIP = playerIP;
    }

    public void setPlayerPort(int playerPort) {
        this.playerPort = playerPort;
    }

    public void stopThread() {
        stop = true;
    }

    public void ccThread() {
        toggle = !toggle;
    }
}
