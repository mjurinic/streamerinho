package com.mjurinic.root.streamerinho;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import java.net.InetAddress;
import java.net.UnknownHostException;

import MyUdpServer.ReceivedPacket;
import MyUdpServer.SerializedObject;
import MyUdpServer.UdpServer;
import messagetypes.IdentifierNotUsable;
import messagetypes.PlayerReady;
import messagetypes.RelayList;
import messagetypes.RelayListRequest;
import messagetypes.RequestStreaming;
import messagetypes.ShutDown;
import messagetypes.SocketIdentifier;
import messagetypes.SourceReady;
import messagetypes.StreamAdvertisement;
import messagetypes.StreamRegister;

public class SourceRetrieverConnection extends AsyncTask<String, String, Boolean> {

    public SourceAsyncResponse delegate = null;

    private Dialog errorDialog;
    private String errorDialogMessage;
    private ProgressDialog pDialog;
    private Context mContext;
    private Thread thrDataFlow;

    private UdpServer server;
    private InetAddress socketIpAddress, publicPlayerIP;
    private int socketPortNumber, publicPlayerPort;
    private ReceivedPacket packet;
    private Object obj;
    private String sourceIdentifier;
    private boolean isRelayActive = false;
    private SourceDataFlow dataFlow;

    private String retrieverIP = "188.166.52.29";

    public SourceRetrieverConnection(Context context) {
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Registering stream...");
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();
    }

    @Override
    protected void onProgressUpdate(String... message) {
        super.onProgressUpdate();

        if (message[0].equals("Dismiss")) pDialog.dismiss();

        pDialog.setMessage(message[0]);
    }

    @Override
    protected Boolean doInBackground(String... params) {
        server = new UdpServer();
        sourceIdentifier = params[0];

        socketIpAddress = DeviceIpAddress.getAddress();
        socketPortNumber = server.getSocketPortNumber();

        //Advertise stream
        publishProgress("Registering stream...");

        StreamAdvertisement streamAdvertisement = new StreamAdvertisement(params[0], socketIpAddress, socketPortNumber);

        try {
            server.sendPacket(InetAddress.getByName(retrieverIP), 9090, SerializedObject.Serialize(streamAdvertisement));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        packet = server.receivePacket();

        System.out.println("Packet received!");

        if (packet == null) {
            errorDialogMessage = "Server did not respond.";

            return false;
        }

        obj = SerializedObject.Deserialize(packet.getData());

        if (obj instanceof StreamRegister) {
            System.out.println("Starting param flow...");

            publishProgress("Stream registered!");

            if (!startParamFlow()) {
                errorDialogMessage = "Shutting down...";

                return false;
            }
            else {
                errorDialogMessage = "Stream finished!";

                return false;
            }
        }

        if (obj instanceof IdentifierNotUsable) {
            errorDialogMessage = "Unable to register stream. Please try again.";

            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (result) delegate.processFinish(result);

        if (pDialog != null) pDialog.dismiss();

        final Boolean res = result;

        if (!result) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

            dialogBuilder.setTitle("Error")
                         .setMessage(errorDialogMessage)
                         .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialog, int which) {
                                 delegate.processFinish(res);
                                 dialog.dismiss();
                             }
                         });

            errorDialog = dialogBuilder.create();
            errorDialog.show();
        }
    }

    /**
     * PLAYER DEVICE WILL CONTACT THE SOURCE DEVICE
     */
    private boolean startParamFlow() {
        server.setSocketTimeOut(10000);

        boolean firstCycle = true;
        boolean isFlowActive = false;

        publishProgress("Waiting for 'player' connection...");

        while (true) {
            packet = server.receivePacket();

            System.out.println("[LISTENING STATE]");

            if (packet != null) {
                obj = SerializedObject.Deserialize(packet.getData());

                if (obj instanceof PlayerReady) {
                    System.out.println("[RECEIVED] MSG_PLAYER_READY");

                    // start stream instead?????
                    //contactPlayer(obj);
                } else if (obj instanceof RequestStreaming && !isFlowActive) {
                    System.out.println("[RECEIVED] MSG_REQUEST_STREAMING");

                    isFlowActive = true;
                    firstCycle = false;

                    RequestStreaming requestStreaming = (RequestStreaming) obj;

                    publicPlayerIP = requestStreaming.getIp();
                    publicPlayerPort = requestStreaming.getPort();

                    /**
                     * Setting the same address:port twice because they are the same
                     * but we don't want to change our sloppy architecture lel
                     */
                    try {
                        dataFlow = new SourceDataFlow(sourceIdentifier, InetAddress.getByName(publicPlayerIP.getHostAddress()), publicPlayerPort, server.getSocketPortNumber() + 1, true);
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }

                    dataFlow.setPlayerIP(publicPlayerIP);
                    dataFlow.setPlayerPort(publicPlayerPort + 1);

                    try {
                        server.sendPacket(InetAddress.getByName(publicPlayerIP.getHostAddress()), publicPlayerPort, SerializedObject.Serialize(new SourceReady(sourceIdentifier)));
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }

                    thrDataFlow = new Thread(dataFlow);
                    thrDataFlow.start();

                    publishProgress("Dismiss");
                } else if (obj instanceof ShutDown) {
                    System.out.println("[RECEIVED] MSG_SHUTTING_DOWN");

                    thrDataFlow.interrupt();

                    return true;
                } else {
                    System.out.println("[RECEIVED] UNKNOWN MESSAGE");
                }
            } else {
                System.out.println("PACKET IS NULL");

                if (firstCycle) {
                    System.out.println("First cycle ParamFlow socket timeout! Player doesn't have a public IP address, starting GoBothWays()");

                    firstCycle = false;

                    /**
                     * ovo je bedara
                     */
                    if (!goBothWays()) {
                        errorDialogMessage = "Server did not respond (GoBothWays)";

                        return false;
                    }
                }
            }
        }
    }

    private void contactPlayer(Object obj) {
        PlayerReady playerInfo = (PlayerReady) obj;
        server.setSocketTimeOut(1000);
        int i = 0;

        while (i++ < 5) {
            server.sendPacket(playerInfo.getPublicAddress().getIp(), playerInfo.getPublicAddress().getPort(), SerializedObject.Serialize(new SourceReady(sourceIdentifier)));
            packet = server.receivePacket();

            if (packet != null) {
                Object receivedObj = SerializedObject.Deserialize(packet.getData());

                if (receivedObj instanceof RequestStreaming) {
                    RequestStreaming requestStreaming = (RequestStreaming) obj;

                    dataFlow = new SourceDataFlow(sourceIdentifier, requestStreaming.getIp(), requestStreaming.getPort(), server.getSocketPortNumber() + 1, true);

                    dataFlow.setPlayerIP(requestStreaming.getIp());
                    dataFlow.setPlayerPort(requestStreaming.getPort() + 1);

                    thrDataFlow = new Thread(dataFlow);
                    thrDataFlow.start();

                    publishProgress("Dismiss");

                    return;
                }
            }
            else {
                errorDialogMessage = "Player did not respond.";

                System.out.println("Player did not respond. (" + (i - 1) + ")");
            }
        }

        if (!goBothWays()) {
            errorDialogMessage = "Server did not respond (GoBothWays)";

            return;
        }
    }

    private boolean goBothWays() {
        System.out.println("Entered goBothWays()");

        InetAddress relayIP, playerIP = null;
        int relayPort, playerPort = 0;

        server.setSocketTimeOut(5000);

        try {
            server.sendPacket(InetAddress.getByName(retrieverIP), 9090, SerializedObject.Serialize(new RelayListRequest()));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        packet = server.receivePacket();

        System.out.println("goBothWays() -> packet received!");

        if (packet != null) {
            Object receivedObj = SerializedObject.Deserialize(packet.getData());

            if (receivedObj instanceof RelayList) {
                //we just gonna take the first server on the list, and start streaming
                RelayList relayList = (RelayList) receivedObj;
                SocketIdentifier relayServer = relayList.getRelayServers().get(0);

                if (relayList.getRelayServers().size() == 0) {
                    System.out.println("No relay servers available. Ending...");

                    return false;
                }

                System.out.println("Relay server: " + relayServer.getIp() + ":" + relayServer.getPort());

                relayIP = relayServer.getIp();
                relayPort = relayServer.getPort();

                /**
                 * Wait for player device to contact the source device
                 */
                server.setSocketTimeOut(20000);

                /**
                 * NAT Map the Relay
                 */
                try {
                    System.out.println("[SENDING] NAT_HELLO");
                    server.sendPacket(InetAddress.getByName(relayIP.getHostAddress()), relayPort, "hello".getBytes());
                    server.sendPacket(InetAddress.getByName(relayIP.getHostAddress()), relayPort, "hello".getBytes());
                    server.sendPacket(InetAddress.getByName(relayIP.getHostAddress()), relayPort, "hello".getBytes());
                }
                catch (UnknownHostException e) {
                    e.printStackTrace();
                }

                publishProgress("Waiting for 'player' connection (2)...");
                packet = server.receivePacket();

                if (packet != null) {
                    receivedObj = SerializedObject.Deserialize(packet.getData());

                    if (receivedObj instanceof RequestStreaming) {
                        System.out.println("[RECEIVED] MSG_REQUEST_STREAMING (goBothWays)");

                        RequestStreaming requestStreaming = (RequestStreaming) receivedObj;

                        playerIP = requestStreaming.getIp();
                        playerPort = requestStreaming.getPort();
                    }
                    else {
                        System.out.println("[RECEIVED] UNKNOWN (goBothWays)");
                    }
                }
                else {
                    System.out.println("Server did not respond (GoBothWays 2)");

                    return false;
                }

                /**
                 * Send FORWARD_MSG to RELAY SERVER - must know relay server ip:port and player ip:port
                 */
                dataFlow = new SourceDataFlow(sourceIdentifier, relayIP, relayPort, server.getSocketPortNumber() + 1, false);

                dataFlow.setPlayerIP(playerIP);
                dataFlow.setPlayerPort(playerPort + 1);

                thrDataFlow = new Thread(dataFlow);
                thrDataFlow.start();

                isRelayActive = true;

                publishProgress("Dismiss");

                return true;
            }
            else {
                System.out.println("goBothWays() -> Unknown packet received.");
            }
        }
        else {
            System.out.println("Server did not respond (GoBothWays 1)");

            return false;
        }

        return false;
    }

    public void stopDataFlow() {
        if (thrDataFlow != null) {
            if (isRelayActive) {
                if (dataFlow != null) {
                    //dataFlow.sendShutDownSignal();
                }
            }
            else {
                try {
                    server.sendPacket(InetAddress.getByName(publicPlayerIP.getHostAddress()), publicPlayerPort, SerializedObject.Serialize(new ShutDown(sourceIdentifier)));
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }

            thrDataFlow.interrupt();
        }
    }
}
