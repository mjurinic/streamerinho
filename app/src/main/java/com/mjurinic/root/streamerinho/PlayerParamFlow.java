package com.mjurinic.root.streamerinho;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.net.InetAddress;
import java.net.UnknownHostException;

import MyUdpServer.ReceivedPacket;
import MyUdpServer.SerializedObject;
import MyUdpServer.UdpServer;
import messagetypes.*;

public class PlayerParamFlow implements Runnable {

    public PlayerAsyncResponse delegate = null;

    private Context mContext;
    private ProgressDialog pDialog;
    private Dialog mDialog;
    private PlayerActivity playerActivity;
    private volatile boolean stop = false;
    private PlayerDataFlow dataFlow;
    private Thread thrDataFlow;

    private String streamIdentifier;
    private InetAddress playerIP, retrieverIP, relayIP, publicPlayerIP, publicSourceIP;
    private int playerPort, retrieverPort, relayPort, publicSourcePort;
    private StreamSourceData sourceData;
    private UdpServer mServer;

    public PlayerParamFlow(Context context, String streamIdentifier) {
        this.mContext = context;
        this.streamIdentifier = streamIdentifier;

        mServer = new UdpServer();

        playerIP = DeviceIpAddress.getAddress();
        playerPort = mServer.getSocketPortNumber();

        try {
            retrieverIP = InetAddress.getByName("188.166.52.29");
            retrieverPort = 9090;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    publicPlayerIP = InetAddress.getByName(DevicePublicIpAddress.getPublicIpAddress());
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Initializing...");
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(true);

        playerActivity = ContextPlayerActivity.getPlayerActivity().get();
    }

    @Override
    public void run() {
        playerActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.show();
            }
        });

        if (!findStreamSource()) {
            showAlertDialog("Unable to find stream source.");

            delegate.processFinish(true);

            return;
        }

        if (!connectToSource()) {
            showAlertDialog("Unable to pair with source device.");

            delegate.processFinish(true);

            return;
        }
    }

    private boolean connectToSource() {
        System.out.println("[CHECKPOINT #1]");
        if (checkDirectConnectivity()) {
            pDialog.cancel();

            thrDataFlow = new Thread(new PlayerDataFlow(streamIdentifier, playerPort + 1));
            thrDataFlow.start();

            listenParamRequests();

            delegate.processFinish(true);

            return true;
        }

        System.out.println("[CHECKPOINT #2]");
        if (goBothWays()) {
            pDialog.cancel();

            thrDataFlow = new Thread(dataFlow);
            thrDataFlow.start();

            listenParamRequests();

            delegate.processFinish(true);

            return true;
        }

        return false;
    }

    private void listenParamRequests() {
        /**
         * Listen for stream shutdown / change params / etc ??
         */
        ReceivedPacket receivedPacket;
        Object receivedObject;

        while (!stop) {
            receivedPacket = mServer.receivePacket();

            if (receivedPacket != null) {
                receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                if (receivedObject instanceof ShutDown) {
                    System.out.println("[RECEIVED] MSG_SHUT_DOWN");
                    return;
                }

                if (receivedObject instanceof Multimedia) {
                    System.out.println("[RECEIVED] MSG_MULTIMEDIA in ParamFlow");

                    /*PleaseForward pleaseForward = new PleaseForward(streamIdentifier, publicSourceIP.toString(), publicSourcePort, SerializedObject.Serialize(new NAThello(playerIP, dataFlow.getNewPortNum())));

                    try {
                        mServer.sendPacket(InetAddress.getByName(relayIP.getHostAddress()), relayPort, SerializedObject.Serialize(pleaseForward));
                        System.out.println("[SENDING] NAT_HELLO PLEASE_FORWARD");
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }*/
                    dataFlow.setFrameData(((Multimedia) receivedObject).getFrameData());
                }
            }
        }

        sendShutDownSignal();

        System.out.println("ENDING listenParamReqeusts");
    }

    /**
     * Sends MSG_REQUEST_RELAY_LIST to the retriever and waits for
     * MSG_RELAY_LIST
     *
     * After receiving a list of relay servers, contacts the first one
     * on the list (EZ WAY) send MSG_REQUEST_STREAMING (to the relay form now one)
     * and wait for MSG_MULTIMEDIA (?) -> start streaming phase
     *
     * @return boolean
     */
    private boolean goBothWays() {
        mServer.setSocketTimeOut(5000);

        updateProgress("Contacting relay server...");

        mServer.sendPacket(retrieverIP, retrieverPort, SerializedObject.Serialize(new RelayListRequest()));

        ReceivedPacket receivedPacket = mServer.receivePacket();

        if (receivedPacket != null) {
            Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

            if (receivedObject instanceof RelayList) {
                System.out.println("[RECEIVED] MSG_RELAY_LIST");

                RelayList relayList = (RelayList) receivedObject;
                SocketIdentifier relayServer = relayList.getRelayServers().get(0);

                if (relayList.getRelayServers().size() == 0) {
                    return false;
                }

                relayIP = relayServer.getIp();
                relayPort = relayServer.getPort();

                RequestStreaming requestStreaming = new RequestStreaming(publicPlayerIP, playerPort);
                PleaseForward forwardRequestStreaming = new PleaseForward(streamIdentifier, publicSourceIP.toString(), publicSourcePort, requestStreaming);

                /**
                 * Also maps NAT
                 */
                try {
                    mServer.setSocketTimeOut(2000);
                    int cnt = 0;

                    dataFlow = new PlayerDataFlow(streamIdentifier, playerPort + 1);
                    dataFlow.setRelayIP(relayIP);
                    dataFlow.setRelayPort(relayPort);
                    dataFlow.setSourceIP(publicSourceIP);
                    dataFlow.setSourcePort(publicSourcePort);
                    dataFlow.MapDeviceToNAT();

                    while (cnt < 15) {
                        System.out.println("[SENDING] MSG_PLEASE_FORWARD");

                        mServer.sendPacket(InetAddress.getByName(relayIP.getHostAddress()), relayPort, SerializedObject.Serialize(forwardRequestStreaming));

                        receivedPacket = mServer.receivePacket();

                        if (receivedPacket != null) {
                            receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                            if (receivedObject instanceof Multimedia) {
                                System.out.println("[RECEIVED] MSG_MULTIMEDIA (goBothWays) - On: " + mServer.getSocketPortNumber());

                                Multimedia tmpMultimedia = (Multimedia) receivedObject;

                                System.out.println("[HINT] Multimedia port: " + tmpMultimedia.getPubPort());

                                PleaseForward pleaseForward = new PleaseForward(streamIdentifier, publicSourceIP.toString(), tmpMultimedia.getPubPort(), SerializedObject.Serialize(new NAThello(playerIP, dataFlow.getNewPortNum())));
                                mServer.sendPacket(InetAddress.getByName(relayIP.getHostAddress()), relayPort, SerializedObject.Serialize(pleaseForward));
                                System.out.println("[SENDING] NAT_HELLO - PLEASE_FORWARD :: contains port: " + dataFlow.getNewPortNum());

                                return true;
                            }
                            else {
                                System.out.println("[RECEIVED] UNKNOWN MESSAGE from: " + receivedPacket.getIp() + ":" + receivedPacket.getPort());
                            }
                        }

                        ++cnt;
                    }
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;
    }

    /**
     * Send MSG_REQUEST_STREAMING 5x every 2000ms
     * until MSG_SOURCE_READY is received
     *
     * If nothing arrives the destination IP address is
     * not public and a 3rd party device is needed (aka. relay server)
     *
     * @return boolean
     */
    private boolean checkDirectConnectivity() {
        mServer.setSocketTimeOut(2000);
        int cnt = 0;

        updateProgress("Contacting source device...");

        while (cnt < 5) {
            System.out.println("[SENDING] MSG_REQUEST_STREAMING --> " + publicSourceIP +":"+ publicSourcePort);

            try {
                mServer.sendPacket(InetAddress.getByName(sourceData.getSourceIdentifier().getPrivateIp().getHostAddress()), sourceData.getSourceIdentifier().getPrivatePort(), SerializedObject.Serialize(new RequestStreaming(playerIP, playerPort)));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            ReceivedPacket receivedPacket = mServer.receivePacket();

            if (receivedPacket != null) {
                Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                if (receivedObject instanceof SourceReady) {
                    System.out.println("[RECEIVED] MSG_SOURCE_READY");

                    return true;
                }
            }

            ++cnt;
        }

        return false;
    }

    /**
     * Send MSG_FIND_STREAM_SOURCE 5x every 2000ms
     * until MSG_STREAM_SOURCE_DATA is received
     *
     * @return boolean
     */
    private boolean findStreamSource() {
        updateProgress("Finding stream source...");

        mServer.setSocketTimeOut(2000);
        int cnt = 0;

        while (cnt < 5) {
            System.out.println("[SENDING] MSG_FIND_STREAM_SOURCE --> " + retrieverIP +":"+ retrieverPort);

            mServer.sendPacket(retrieverIP, retrieverPort, SerializedObject.Serialize(new FindStreamSource(streamIdentifier)));

            ReceivedPacket receivedPacket = mServer.receivePacket();

            if (receivedPacket != null) {
                Object receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                if (receivedObject instanceof StreamSourceData) {
                    sourceData = (StreamSourceData) receivedObject;

                    publicSourceIP = sourceData.getSourceIdentifier().getIp();
                    publicSourcePort = sourceData.getSourceIdentifier().getPort();

                    System.out.println("[RECEIVED] MSG_SOURCE_DATA");

                    return true;
                }

                if (receivedObject instanceof IdentifierNotUsable) {
                    System.out.println("[RECEIVED] MSG_IDENTIFIER_NOT_USABLE");

                    return false;
                }
            }

            ++cnt;
        }

        return false;
    }

    private void updateProgress(String message) {
        final String mMessage = message;

        playerActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.setMessage(mMessage);
            }
        });
    }

    private void showAlertDialog(String message) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

        dialogBuilder.setTitle("Error")
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delegate.processFinish(false);
                        dialog.dismiss();
                    }
                });

        playerActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDialog = dialogBuilder.create();
                mDialog.show();
            }
        });
    }

    public void stopThread() {
        if (dataFlow != null) {
            dataFlow.stopThread();
        }

        stop = true;
    }

    public void sendShutDownSignal() {
        if (relayIP == null) {
            try {
                mServer.sendPacket(InetAddress.getByName(sourceData.getSourceIdentifier().getPrivateIp().getHostAddress()), sourceData.getSourceIdentifier().getPrivatePort(), SerializedObject.Serialize(new ShutDown(streamIdentifier)));
                System.out.println("[SENDING] MSG_SHUTTING_DOWN [1]");
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        } /*else {
            PleaseForward pleaseForward = new PleaseForward(identifier, sourceIP.toString().replace("/", ""), sourcePort, new ShutDown(identifier));
            mServer.sendPacket(relayIP, relayPort, SerializedObject.Serialize(pleaseForward));

            System.out.println("[SENDING] MSG_SHUTTING_DOWN [2]");
        }*/
    }
}
