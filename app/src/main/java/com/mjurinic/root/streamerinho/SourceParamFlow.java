package com.mjurinic.root.streamerinho;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.net.InetAddress;
import java.net.UnknownHostException;

import MyUdpServer.ReceivedPacket;
import MyUdpServer.SerializedObject;
import MyUdpServer.UdpServer;
import messagetypes.IdentifierNotUsable;
import messagetypes.NAThello;
import messagetypes.PleaseForward;
import messagetypes.RelayList;
import messagetypes.RelayListRequest;
import messagetypes.RequestStreaming;
import messagetypes.ShutDown;
import messagetypes.SocketIdentifier;
import messagetypes.SourceReady;
import messagetypes.StreamAdvertisement;
import messagetypes.StreamRegister;

public class SourceParamFlow implements Runnable {

    public SourceAsyncResponse delegate = null;

    private Context mContext;
    private ProgressDialog pDialog;
    private Dialog mDialog;
    private Thread thrDataFlow;
    private SourceDataFlow dataFlow;
    private volatile boolean stop = false;
    private StreamActivity streamActivity;

    private String sourceIdentifier;
    private UdpServer mServer;
    private InetAddress sourceAddress, playerAddress, relayAddress = null;
    private int sourcePort, playerPort, relayPort;
    private ReceivedPacket receivedPacket;
    private Object receivedObject;
    private final String retrieverIp = "188.166.52.29";
    private final int retrieverPort = 9090;

    public SourceParamFlow(Context context, String sourceIdentifier) {
        mContext = context;
        this.sourceIdentifier = sourceIdentifier;

        mServer = new UdpServer();
        sourceAddress = DeviceIpAddress.getAddress();
        sourcePort = mServer.getSocketPortNumber();

        System.out.println("[SERVER +] Binding socket to port: " + sourcePort);

        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Initializing...");
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(true);

        streamActivity = ContextSourceActivity.getStreamActivity().get();
    }

    @Override
    public void run() {
        streamActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.show();
            }
        });

        /**
         * Advertising stream
         */
        updateProgress("Registering...");
        StreamAdvertisement streamAdvertisement = new StreamAdvertisement(sourceIdentifier, sourceAddress, sourcePort);

        try {
            mServer.sendPacket(InetAddress.getByName(retrieverIp), retrieverPort, SerializedObject.Serialize(streamAdvertisement));
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
        }

        receivedPacket = mServer.receivePacket();

        if (receivedPacket != null) {
            receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

            if (receivedObject instanceof StreamRegister) {
                System.out.println("[RECEIVED] MSG_STREAM_REGISTERED");

                /**
                 * Waiting for direct connection
                 */
                if (waitForDirectConnection()) {
                    pDialog.dismiss();

                    dataFlow = new SourceDataFlow(sourceIdentifier, playerAddress, playerPort + 1, true);

                    thrDataFlow = new Thread(dataFlow);
                    thrDataFlow.start();

                    listenParamRequests();

                    delegate.processFinish(true);

                    return;
                }

                /**
                 * GoBothWays -> device-relay-device
                 */
                if (goBothWays()) {
                    pDialog.dismiss();

                    dataFlow = new SourceDataFlow(sourceIdentifier, relayAddress, relayPort, sourcePort + 1, false);
                    dataFlow.setPlayerIP(playerAddress);
                    dataFlow.setPlayerPort(playerPort); //+1 ???

                    thrDataFlow = new Thread(dataFlow);
                    thrDataFlow.start();

                    listenParamRequests();

                    delegate.processFinish(true);

                    return;
                }
            }

            if (receivedObject instanceof IdentifierNotUsable) {
                System.out.println("[RECEIVED] MSG_IDENTIFIER_NOT_USABLE");
                showAlertDialog("Identifier not usable!");
            }
        }
    }

    private void listenParamRequests() {
        while (!stop) {
            receivedPacket = mServer.receivePacket();

            if (receivedPacket != null) {
                receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                if (receivedObject instanceof ShutDown) {
                    System.out.println("[RECEIVED] MSG_SHUT_DOWN");
                    return;
                }

                if (receivedObject instanceof NAThello) {
                    System.out.println("[RECEIVED] NAT_HELLO in SourceParamFlow");

                    dataFlow.ccThread();
                    dataFlow.setPlayerPort(((NAThello) receivedObject).getPort());
                    dataFlow.ccThread();
                }
            }
        }

        sendShutDownSignal();
    }

    private boolean goBothWays() {
        mServer.setSocketTimeOut(5000);

        updateProgress("Fetching relay list...");

        RelayListRequest relayListRequest = new RelayListRequest();

        try {
            mServer.sendPacket(InetAddress.getByName(retrieverIp), retrieverPort, SerializedObject.Serialize(relayListRequest));
            System.out.println("[SENDING] MSG_REQUEST_RELAY_LIST");
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
        }

        receivedPacket = mServer.receivePacket();

        if (receivedPacket != null) {
            receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

            if (receivedObject instanceof RelayList) {
                System.out.println("[RECEIVED] MSG_RELAY_LIST");
                updateProgress("Contacting relay server...");

                RelayList relayList = (RelayList) receivedObject;

                if (relayList.getRelayServers().size() > 0) {
                    SocketIdentifier relayServer = relayList.getRelayServers().get(0);

                    relayAddress = relayServer.getIp();
                    relayPort = relayServer.getPort();

                    mServer.setSocketTimeOut(26250); // player module sends 15 packets every 1750ms

                    try {
                        mServer.sendPacket(InetAddress.getByName(relayAddress.getHostAddress()), relayPort, "NAT_HELLO".getBytes());
                        System.out.println("[SENDING] NAT_HELLO");
                    }
                    catch (UnknownHostException e) {
                        e.printStackTrace();
                    }

                    receivedPacket = mServer.receivePacket();

                    if (receivedPacket != null) {
                        receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

                        if (receivedObject instanceof RequestStreaming) {
                            System.out.println("[RECEIVED] MSG_REQUEST_STREAMING [2]");

                            RequestStreaming requestStreaming = (RequestStreaming) receivedObject;

                            playerAddress = requestStreaming.getIp();
                            playerPort = requestStreaming.getPort();

                            return true;
                        }
                        else {
                            System.out.println("[RECEIVED] UNKNOWN MESSAGE from: " + receivedPacket.getIp() + ":" + receivedPacket.getPort());
                        }
                    }
                    else {
                        showAlertDialog("Player did not respond.");

                        return false;
                    }
                }
                else {
                    showAlertDialog("No relays available!");

                    return false;
                }
            }
        }

        return false;
    }

    private boolean waitForDirectConnection() {
        mServer.setSocketTimeOut(5000);

        updateProgress("Waiting for connection...");

        receivedPacket = mServer.receivePacket();

        if (receivedPacket != null) {
            receivedObject = SerializedObject.Deserialize(receivedPacket.getData());

            if (receivedObject instanceof RequestStreaming) {
                System.out.println("[RECEIVED] MSG_REQUEST_STREAMING [1]");

                RequestStreaming requestStreaming = (RequestStreaming) receivedObject;
                SourceReady sourceReady = new SourceReady(sourceIdentifier);

                playerAddress = requestStreaming.getIp();
                playerPort = requestStreaming.getPort();

                try {
                    mServer.sendPacket(InetAddress.getByName(playerAddress.getHostAddress()), playerPort, SerializedObject.Serialize(sourceReady));
                    System.out.println("[SENDING] MSG_SOURCE_READY [1]");

                    return true;
                }
                catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;
    }

    private void updateProgress(String message) {
        final String mMessage = message;

        streamActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.setMessage(mMessage);
            }
        });
    }

    private void showAlertDialog(String message) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

        dialogBuilder.setTitle("Error")
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delegate.processFinish(false);
                        dialog.dismiss();
                    }
                });

        streamActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDialog = dialogBuilder.create();
                mDialog.show();
            }
        });
    }

    public void stopThread() {
        if (dataFlow != null) {
            dataFlow.stopThread();
        }

        stop = true;
    }

    public void sendShutDownSignal() {
        if (relayAddress == null) {
            try {
                mServer.sendPacket(InetAddress.getByName(playerAddress.getHostAddress()), playerPort, SerializedObject.Serialize(new ShutDown(sourceIdentifier)));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            System.out.println("[SENDING] MSG_SHUTTING_DOWN [1]");
        }
        else {
            PleaseForward pleaseForward = new PleaseForward(sourceIdentifier, playerAddress.toString().replace("/", ""), playerPort, new ShutDown(sourceIdentifier));

            mServer.sendPacket(relayAddress, relayPort, SerializedObject.Serialize(pleaseForward));

            System.out.println("[SENDING] MSG_SHUTTING_DOWN [2]");
        }
    }
}
