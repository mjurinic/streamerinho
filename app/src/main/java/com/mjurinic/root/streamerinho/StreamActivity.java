package com.mjurinic.root.streamerinho;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.Random;

public class StreamActivity extends Activity implements SourceAsyncResponse {

    private Camera mCamera;
    private CameraPreview mPreview;
    private AlertDialog mDialog;
    private FrameLayout cameraPreview;

    private Thread thrParamFlow;
    private SourceParamFlow sourceParamFlow;
    private SourceRetrieverConnection sourceRetrieverConnection;
    private String streamIdentifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream);

        /*sourceRetrieverConnection = new SourceRetrieverConnection(StreamActivity.this);
        sourceRetrieverConnection.delegate = this;*/

        initCamera();
        showConnectionCode();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);

            mPreview.getHolder().removeCallback(mPreview);

            mCamera.release();
            mCamera = null;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mCamera == null) {
            initCamera();
        }
    }

    @Override
    public void processFinish(Boolean response) {
        if (sourceParamFlow != null) {
            sourceParamFlow.stopThread();
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        sourceParamFlow.stopThread();
    }

    private void initFlowThread() {
        ContextSourceActivity.updateActivity(this);

        sourceParamFlow = new SourceParamFlow(StreamActivity.this, streamIdentifier);
        sourceParamFlow.delegate = this;
    }

    private void showConnectionCode() {
        streamIdentifier = codeGenerator();

        mDialog = dialogManager(streamIdentifier);
        mDialog.show();
    }

    private String codeGenerator() {
        String characters = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789";
        char[] ret = new char[5];
        Random rng = new Random();

        for (int i = 0; i < ret.length; ++i) {
            ret[i] = characters.charAt(rng.nextInt(characters.length()));
        }

        return new String(ret);
    }

    private AlertDialog dialogManager(String connectionCode) {
        View dialogConnectionCode = StreamActivity.this.getLayoutInflater().inflate(R.layout.dialog_connection_code, null);

        TextView tvConnectionCode = (TextView) dialogConnectionCode.findViewById(R.id.connection_code);
        tvConnectionCode.setText(connectionCode);

        AlertDialog.Builder builder = new AlertDialog.Builder(StreamActivity.this);

        builder.setView(dialogConnectionCode)
               .setPositiveButton("Connect", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       dialog.dismiss();
                       //sourceRetrieverConnection.execute(streamIdentifier);

                       initFlowThread();

                       thrParamFlow = new Thread(sourceParamFlow);
                       thrParamFlow.start();
                   }
               })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       dialog.dismiss();
                       finish();
                   }
               });

        return builder.create();
    }

    private void initCamera() {
        mCamera = getCameraInstance();
        mPreview = new CameraPreview(this, mCamera);

        Camera.Parameters cameraParams = mCamera.getParameters();
        cameraParams.setPreviewSize(640, 360);

        mCamera.setParameters(cameraParams);

        cameraPreview = (FrameLayout) findViewById(R.id.camera_preview);
        cameraPreview.addView(mPreview);
    }

    private Camera getCameraInstance() {
        Camera c = null;

        try {
            c = Camera.open();
        }
        catch (Exception e) {
            // Camera is not available
        }

        return c;
    }
}
