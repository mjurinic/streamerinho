package com.mjurinic.root.streamerinho;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class PlayerActivity extends Activity implements PlayerAsyncResponse {

    private String streamIdentifier;

    private ImageView ivPlayer;
    private AlertDialog mDialog;
    private PlayerParamFlow playerParamFlow;
    private Thread thrParamFlow;
    private boolean isBackPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        ivPlayer = (ImageView) findViewById(R.id.ivPlayer);

        showInputDialog();
    }

    @Override
    public void processFinish(Boolean response) {
        if (isBackPressed) return;

        if (playerParamFlow != null) {
            playerParamFlow.stopThread();
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        isBackPressed = true;

        if (playerParamFlow != null) {
            playerParamFlow.stopThread();
        }

        finish();
    }

    private void initFlowThread() {
        ContextPlayerActivity.updateActivity(this);

        playerParamFlow = new PlayerParamFlow(PlayerActivity.this, streamIdentifier);
        playerParamFlow.delegate = this;
    }

    public ImageView getIvPlayer() {
        return ivPlayer;
    }

    private void showInputDialog() {
        View dialogConnectionCode = PlayerActivity.this.getLayoutInflater().inflate(R.layout.dialog_enter_connection_code, null);
        final EditText etConnectionCode = (EditText) dialogConnectionCode.findViewById(R.id.etPlayerConnection);

        AlertDialog.Builder builder = new AlertDialog.Builder(PlayerActivity.this);

        builder.setView(dialogConnectionCode)
                .setPositiveButton("connect", null)
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        mDialog = builder.create();
        mDialog.show();

        mDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                streamIdentifier = etConnectionCode.getText().toString();

                if (streamIdentifier.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Connection code should be 5 characters long", Toast.LENGTH_SHORT).show();
                } else {
                    mDialog.dismiss();

                    initFlowThread();

                    thrParamFlow = new Thread(playerParamFlow);
                    thrParamFlow.start();
                }
            }
        });
    }
}
