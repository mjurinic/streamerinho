package com.mjurinic.root.streamerinho;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class DeviceIpAddress {

    private static InetAddress getLocalAddress() {
        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();

            while (e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();

                while (ee.hasMoreElements()) {
                    InetAddress i = (InetAddress) ee.nextElement();
                    String substr = (i.getHostAddress()).substring(0, 3);

                    if (!substr.equals("127") && !substr.equals("0:0") && !substr.equals("fe8")) {
                        return i;
                    }
                }
            }
        } catch (SocketException e1) {
            e1.printStackTrace();
        }

        return null;
    }

    public static InetAddress getAddress() {
        return getLocalAddress();
    }
}
