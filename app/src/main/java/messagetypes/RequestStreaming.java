package messagetypes;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Contains public ip:port of the player device
 */

public class RequestStreaming extends SocketIdentifier implements Serializable {

    //multimedia params???

    public RequestStreaming(InetAddress ip, int port) {
        super(ip, port);
    }
}
