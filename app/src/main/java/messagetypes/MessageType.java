package messagetypes;

public enum MessageType {

    MSG_PING                    ((byte) 0x01),
    MSG_PONG                    ((byte) 0x02),
    MSG_PONG_REG_REQ            ((byte) 0x03),
    MSG_STREAM_ADVERTISEMENT    ((byte) 0x04),
    MSG_STREAM_REGISTERED       ((byte) 0x05),
    MSG_IDENTIFIER_NOT_USABLE   ((byte) 0x06),
    MSG_FIND_STREAM_SOURCE      ((byte) 0x07),
    MSG_STREAM_SOURCE_DATA      ((byte) 0x08),
    MSG_STREAM_REMOVE           ((byte) 0x09),
    MSG_MULTIMEDIA              ((byte) 0x0A),
    MSG_REQUEST_STREAMING       ((byte) 0x0B),
    MSG_FORWARD_PLAYER_READY    ((byte) 0x0C),
    MSG_PLAYER_READY            ((byte) 0x0D),
    MSG_SOURCE_READY            ((byte) 0x0E),
    MSG_REQ_RELAY_LIST          ((byte) 0x0F),
    MSG_RELAY_LIST              ((byte) 0x10),
    MSG_SHUTTING_DOWN           ((byte) 0x11),
    MSG_PLEASE_FORWARD          ((byte) 0x12),
    MSG_REGISTER_FORWARDING     ((byte) 0x13);

    private byte type;

    private MessageType(byte type) {
        this.type = type;
    }

    public byte getValue() {
        return this.type;
    }
}
