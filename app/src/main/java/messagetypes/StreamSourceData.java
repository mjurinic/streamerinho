package messagetypes;

import java.io.Serializable;

/**
 * Information filled by the server
 * Server sends a request message in order to get
 * both private and public IP address and port number of the source
 * Also contains the public IP address and port number of the player
 */
public class StreamSourceData implements Serializable {

    private String identifier;
    private SocketIdentifier playerIdentifier;
    private OnlineStreamInfo sourceIdentifier;

    public StreamSourceData(SocketIdentifier playerIdentifier, OnlineStreamInfo sourceIdentifier) {
        this.playerIdentifier = playerIdentifier;
        this.sourceIdentifier = sourceIdentifier;
    }

    public String getIdentifier() { return identifier; }

    public SocketIdentifier getPlayerIdentifier() {
        return playerIdentifier;
    }

    public OnlineStreamInfo getSourceIdentifier() {
        return sourceIdentifier;
    }

    public void setIdentifier(String identifier) { this.identifier = identifier; }
}