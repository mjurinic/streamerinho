package messagetypes;

import java.io.Serializable;

/**
 * Created by root on 21.07.15..
 */
public class FindStreamSource implements Serializable {

    private byte type;
    private String identifier;

    public FindStreamSource(String identifier) {
        type = MessageType.MSG_FIND_STREAM_SOURCE.getValue();
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }
}
