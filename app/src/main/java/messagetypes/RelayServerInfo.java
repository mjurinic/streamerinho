package messagetypes;

import java.net.InetAddress;

/**
 * Created by root on 20.07.15..
 */
public class RelayServerInfo {

    private InetAddress ip;
    private int port;

    public RelayServerInfo(InetAddress ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public InetAddress getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
