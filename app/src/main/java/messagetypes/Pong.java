package messagetypes;

import java.io.Serializable;

/**
 * Created by root on 20.07.15..
 */
public class Pong implements Serializable {
    
    /*
     * Pinging with 64 bytes
     */

    private byte[] data;

    public Pong() {
        data = new byte[64];
    }
}
