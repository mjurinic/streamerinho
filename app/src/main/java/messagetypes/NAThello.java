package messagetypes;

import java.io.Serializable;
import java.net.InetAddress;

/**
 * Created by root on 28.09.15..
 */
public class NAThello implements Serializable {

    private InetAddress ip;
    private int port;

    public NAThello(InetAddress ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public InetAddress getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }
}
