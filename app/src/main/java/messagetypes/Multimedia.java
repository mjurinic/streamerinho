package messagetypes;

import java.io.Serializable;

/**
 * stream ID, packet num., timestamp, frame data
 */

public class Multimedia implements Serializable {

    private String identifier;
    private byte[] frameData;
    private int pubPort;

    public Multimedia(String identifier, byte[] frameData) {
        this.identifier = identifier;
        this.frameData = frameData;
    }

    public String getIdentifier() { return identifier; }

    public byte[] getFrameData() { return frameData; }

    public void setPubPort(int port) {
        pubPort = port;
    }

    public int getPubPort() {
        return pubPort;
    }
}
